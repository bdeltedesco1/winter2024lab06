import java.util.Scanner;
public class LuckyCardGameApp{
	
	public static void main(String[] args){
		GameManager game = new GameManager();
		int totalPoints = 0;
		boolean winner = false;
		int round = 0;
		
		System.out.println("*******************************");
		System.out.println("Welcome to the Lucky Card Game!");
		
		while (game.getNumberOfCards() > 0){
			Scanner reader = new Scanner(System.in);
			
			round += 1;
			System.out.println("*******************************");
			System.out.println("Round " + round);
			
			totalPoints += game.calculatePoints();
			
			System.out.println(game);
			
			if (totalPoints >= 5){
				winner = true;
				break;
			}
			
			System.out.println("You have " + totalPoints + " points");
			
			System.out.println("There are " + game.getNumberOfCards() + " cards remaining");
			
			game.dealCards();
			
			System.out.println("Press any key to continue");
			String temp = reader.next();
			
		}
		if (winner){
			System.out.println("Congratulations! You won!");
		}
		else{
			System.out.println("Unfortunately, you lost");
		}
		System.out.println("Your final score was " + totalPoints);
		
	}
}