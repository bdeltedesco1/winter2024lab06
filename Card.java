public class Card{
	
	private String suit;
	private String name;
	private int value;
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public Card(String suit, String name, int value){
		this.suit = suit;
		this.name = name;
		this.value = value;
	}
	
	public String toString(){
		return this.suit + " " + this.name; // + " " + this.value;
	}
}