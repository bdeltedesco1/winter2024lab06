public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
		
	}
	
	public String toString(){
		return "----------------------------\nCenter Card: " + this.centerCard + "\nPlayer Card: " + this.playerCard + "\n----------------------------";
	}
	
	public void dealCards(){
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	public int calculatePoints(){
		if (centerCard.getValue() == playerCard.getValue()){
			return 4;
		}
		else if (centerCard.getSuit().equals(playerCard.getSuit())){
			return 2;
		}
		else{
			return -1;
		}
	}
}